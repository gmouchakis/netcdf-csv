/**
 * 
 */
package gr.demokritos.iit.netcdfcsv;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import ucar.ma2.Array;
import ucar.ma2.IndexIterator;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

/**
 * @author Yiannis Mouchakis
 *
 * Exports CSV from NetCDF data. For each variable that is not a dimension variable a different CSV file is produced.
 *
 */
public class NetCDFToCSV {
	
	private String netcdf_path;
	private String csv_directory;
	
	private boolean compress = false;
	private boolean in_memory_export = false;
	
	private String str_null = "\\N";
	
	/**
	 * 
	 * @param netcdf_path path of the netcdf file
	 * @param csv_directory directory to store the produced CSV files. Filenames will be {netcdf_name}_{variable_name}.csv
	 */
	public NetCDFToCSV(String netcdf_path, String csv_directory) {
		this.netcdf_path = netcdf_path;
		this.csv_directory = csv_directory;
	}
	
	
	/**
	 * @return true if output is going to be compressed
	 */
	public boolean usesCompress() {
		return compress;
	}



	/**
	 * @param compress true to compress the output. by default false.
	 */
	public void setCompress(boolean compress) {
		this.compress = compress;
	}


	/**
	 * @return the in_memory_export setting. by default false
	 */
	public boolean usesInMemoryExport() {
		return in_memory_export;
	}


	/**
	 * @param in_memory_export set true if you want to load data in memory before write so export will be faster.
	 * If dataset is too big you will run out of memory (depends on allocated memory). 
	 * By default in_memory_export = false
	 */
	public void setInMemoryExport(boolean in_memory_export) {
		this.in_memory_export = in_memory_export;
	}
	
	/**
	 * 
	 * @return the value for null in produced file that will be used to represent fill values.
	 * by default "\\N"
	 */
	public String getStrNull() {
		return this.str_null;
	}

	
	/**
	 * 
	 * @param str_null the value for null in produced file that will be used to represent fill values.
	 * by default "\\N"
	 */
	public void setStrNull(String str_null) {
		this.str_null = str_null;
	}

	/**
	 * 
	 * Exports netcdf data to CSV. If all data can be loaded on memory use setInMemoryExport(true) before this
	 * 
	 * @throws IOException
	 * @throws InvalidRangeException
	 */
	public void export() throws IOException, InvalidRangeException {
		
		NetcdfFile ncfile = NetcdfFile.open(netcdf_path);

		List<Variable> var_list = ncfile.getVariables();
		List<Dimension> dim_list = ncfile.getDimensions();
		
		writeDimensionWithNoData(dim_list, ncfile);

		List<Variable> measurement_variables = new ArrayList<Variable>();

		for (Variable variable : var_list) {
			if ( ! isDimensionVar(variable, dim_list)) {
				measurement_variables.add(variable);//discover measurement variables
			} else {
				writeDimensionData(variable);//or write codelist
			}
		}
		
		
		for (Variable measurement_variable : measurement_variables) {
			
			String output_filepath = csv_directory + Paths.get(netcdf_path).getFileName() + "_" + measurement_variable.getShortName() + ".csv";
			
			BufferedWriter writer = getWriter(output_filepath);
			
			Object fill_value = getFillValue(measurement_variable);
			
			if (in_memory_export) {//load values in memory and start writing.
				Array results = measurement_variable.read();
				long row = 0L;//row number
				IndexIterator it = results.getIndexIterator();
				while(it.hasNext()) {
					Object result = it.next();
					int[] indices = it.getCurrentCounter();
					writeDimensionIndices(row, indices, writer);
					//write value or getStrNull() result if fill value
					writer.write(getValue(fill_value, result));
					writer.newLine();
					row++;
				}
				
			} else {//read values one by one and write.
				
				List<Dimension> dims = measurement_variable.getDimensions();
				int n = dims.size();
				int[] indices = new int[n];
				long[] ranges = new long[n];
				int i = 0;
				for (Dimension dim : dims) {
					ranges[i] = dim.getLength();
					i++;
				}
						
				// writes observations
				// Start off at indices = [0,...,0]
				long row = 0L;//row number
				do {
					writeDimensionIndices(row, indices, writer);
					writeMeasurementData(indices, measurement_variable, fill_value, writer);
					advanceIndices(n, indices, ranges);
					row++;
				}
				while( ! allMaxed(n, indices, ranges) );
					//do last one
					writeDimensionIndices(row, indices, writer);
					writeMeasurementData(indices, measurement_variable, fill_value, writer);
				}
			
			writer.close();
			
		}
		
		ncfile.close();
		
	}
	
	/**
	 * 
	 * writes dimension index for each variable
	 * 
	 * @param row the row number to be written
	 * @param indices array the holds current index position
	 * @param writer output writer
	 * @throws IOException
	 */
	private void writeDimensionIndices(Long row, int[] indices, BufferedWriter writer) throws IOException {
		writer.write(row.toString() + ",");
		for (int i=0; i<indices.length; i++) {
			writer.write(indices[i] + ",");
		}
	}
	
	/**
	 * 
	 * @param indices array the holds current index position
	 * @param measurement_variable the variable to get data from
	 * @param dim_data_ordered List that holds all dimension data
	 * @param writer
	 * @throws IOException
	 * @throws InvalidRangeException
	 */
	private void writeMeasurementData( int[] indices, Variable measurement_variable, Object fill_value, BufferedWriter writer)
	throws IOException, InvalidRangeException
	{
		
		String sectionSpec = "";//used to read values in the grid
		for (int i = 0; i < indices.length - 1; i++) {
			sectionSpec += indices[i] + ":" + indices[i] + ", ";
		}
		sectionSpec += indices[indices.length - 1] + ":" + indices[indices.length - 1];
		
		//write measurement
		Array result = measurement_variable.read(sectionSpec);
		writer.write(getValue(measurement_variable, getValue(fill_value, result.getObject(0))));
		writer.newLine();
	}
	
	
	private void writeDimensionData(Variable var) throws IOException {
		String output_filepath = csv_directory + Paths.get(netcdf_path).getFileName() + "_" + var.getShortName() + ".csv";
		
		BufferedWriter writer = getWriter(output_filepath);
		
		IndexIterator it = var.read().getIndexIterator();
		long row_no = 0L;
		while(it.hasNext()) {
			writer.write(row_no++ + "," + getValue(getFillValue(var), it.next().toString()));
			writer.newLine();
		}
		writer.close();
	}
	
	/**
	 * 
	 * Create CSV file for each dimension that is associated with values, just an index and write the index.
	 * 
	 * @param dim_list dataset dimensions
	 * @param ncfile input file
	 * @throws IOException
	 */
	private void writeDimensionWithNoData(List<Dimension> dim_list, NetcdfFile ncfile) throws IOException {
		
		for (Dimension dim : dim_list) {
			if (ncfile.findVariable(dim.getShortName()) == null) {//if dimensions is not also a variable
				
				String output_filepath = csv_directory + Paths.get(netcdf_path).getFileName() + "_" + dim.getShortName() + ".csv";

				BufferedWriter writer = getWriter(output_filepath);
				
				for (int row_no=0; row_no<dim.getLength(); row_no++) {
					writer.write(new Integer(row_no).toString());
					writer.newLine();
				}
				
				writer.close();
			}
		}
	}
	
	/**
	 * Advances 'indices' to the next in sequence.
	 * @param n
	 * @param indices
	 * @param ranges
	 */
    private void advanceIndices(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i]+1 == ranges[i]) {
                indices[i] = 0;
            }
            else {
                indices[i] += 1;
                break;
            }
        }

    }

    /**
     * Tests if indices are in final position.
     * @param n
     * @param indices
     * @param ranges
     * @return
     */
    private boolean allMaxed(int n, int[] indices, long[] ranges) {

        for(int i=n-1; i>=0; i--) {
            if(indices[i] != ranges[i]-1) {
                return false;
            }
        }
        return true;

    }
    
    /**
     * Private helper checking if a variable is a dimension.
     * 
     * @param variable
     * @param dim_list
     */
	private boolean isDimensionVar(Variable variable, List<Dimension> dim_list)
	{
		for( Dimension dimension : dim_list ) {
			String dimName = dimension.getShortName();
			if( (dimName != null) && dimName.equals(variable.getShortName()) )
			{ return true; }
		}
		return false;
	}
	
	/**
	 * 
	 * Checks a value against the fill value of a variable and returns the String representation of the value.
	 * 
	 * @param fill_value netcdf fill value
	 * @param value the value to check against the fill value
	 * @return string defined for null with setStrNull if value equals the fill value, 
	 * the string representation of the value otherwise
	 */
	private String getValue(Object fill_value, Object value) {
		if (value.equals(fill_value)) {
			return getStrNull();
		} else {
			return value.toString();
		}
	}
	
	
	/**
	 * 
	 * Get the netcdf fill value if any
	 * 
	 * @param variable the variable to extract the fill value
	 * @return the fill value or null if not set
	 */
	private Object getFillValue(Variable variable) {
		Object fill_value = null;
		Attribute fill_value_attr = variable.findAttribute("_FillValue");
		if (fill_value_attr != null) {
			fill_value = fill_value_attr.getValue(0);
		}
		return fill_value;
	}
	
	
	/**
	 * 
	 * @param output_filepath where to write
	 * @return compressed FileOuputStream if compress = true or simple FileWriter if false. 
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private BufferedWriter getWriter(String output_filepath) throws UnsupportedEncodingException, FileNotFoundException, IOException {

		if (compress) {
			output_filepath = output_filepath + ".gz";	
			return new BufferedWriter(
					new OutputStreamWriter(
							new GZIPOutputStream(
									new FileOutputStream(output_filepath)), "UTF-8"));
		} else {
			return  new BufferedWriter( new FileWriter(output_filepath) );
		}
	}

	
	/**
	 * prints help message
	 * @param options the available options
	 */
	private static void help(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("<application name>", options, true);
	}

	/**
	 * @param args
	 * @throws InvalidRangeException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InvalidRangeException {
		
		Options options = new Options();
		
		Option help = Option.builder("h")
				.desc("prints help message")
				.longOpt("help")
				.build();
		options.addOption(help);
		
		Option input = Option.builder("i")
				.desc("path to netcdf")
				.longOpt("input")
				.hasArg()
				.required()
				.build();
		options.addOption(input);
		
		Option output = Option.builder("o")
				.desc("directory to store output files")
				.longOpt("output")
				.hasArg()
				.required()
				.build();
		options.addOption(output);
		
		Option memory = Option.builder("m")
				.desc("load data in memory for faster export")
				.longOpt("memory")
				.build();
		options.addOption(memory);
		
		Option compress = Option.builder("c")
				.desc("compress output files")
				.longOpt("compress")
				.build();
		options.addOption(compress);
		
		
		CommandLineParser parser = new DefaultParser();
		
		try {
			
			CommandLine start_cmdl = parser.parse(new Options().addOption(help), args, true);
			if (start_cmdl.hasOption(help.getOpt())) {
				
				help(options);
				
			} else {
				
				CommandLine cmdl = parser.parse(options, args);
				
				NetCDFToCSV netcdfToCSV = new NetCDFToCSV(
						cmdl.getOptionValue(input.getOpt()),
						cmdl.getOptionValue(output.getOpt())
						);
				
				if (cmdl.hasOption(memory.getOpt())) {
					netcdfToCSV.setInMemoryExport(true);
				}
				
				if (cmdl.hasOption(compress.getOpt())) {
					netcdfToCSV.setCompress(true);
				}
				
				netcdfToCSV.export();
				
			}
		} catch (ParseException e) {
			System.err.println("Parsing failed, correct usage is:");
			help(options);
			throw new IllegalArgumentException(e);
		}

	}

}
